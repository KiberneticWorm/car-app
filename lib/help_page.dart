import 'package:car_app/strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HelpPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var classInfo = Strings.aboutCarClasses;

    return Scaffold(
        appBar: AppBar(
          title: Text(Strings.helpTitle),
        ),
        body: Padding(
          padding: EdgeInsets.all(10.0),
          child: ListView.builder(
            itemCount: classInfo.length ~/ 2,
            itemBuilder: (BuildContext context, int position) {
              return Container(
                margin: EdgeInsets.only(bottom: 20),
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 6),
                      child: Text(classInfo[position * 2],
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold)),
                    ),
                    Text(classInfo[position * 2 + 1])
                  ],
                ),
              );
            },
          ),
        ));
  }
}
